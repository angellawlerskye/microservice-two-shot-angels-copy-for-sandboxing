import React, { Component } from 'react';

class ShoeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: '',
      model_name: '',
      color: '',
      picture_url: '',
      bin: '', // Initialize bin as an empty string
      bins: [], // Initialize an array to store the list of bins
    };
  }

  async componentDidMount() {
    try {
      // Fetch the list of bins from your API
      const response = await fetch('http://localhost:8100/api/bins/');
      if (!response.ok) {
        throw new Error(`Network response was not ok (Status: ${response.status})`);
      }
      const data = await response.json();
      if (!data.bins) {
        throw new Error('Bins data not available');
      }
      this.setState({ bins: data.bins });
    } catch (error) {
      console.error('Error fetching bins:', error);
    }
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const { manufacturer, model_name, color, picture_url, bin } = this.state;

    try {
      // Send a POST request to the API endpoint to create a new shoe
      const response = await fetch('http://localhost:8080/api/shoes/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          manufacturer,
          model_name,
          color,
          picture_url,
          bin,
        }),
      });

      if (!response.ok) {
        throw new Error(`Network response was not ok (Status: ${response.status})`);
      }

      // Reset the form after successfully creating the shoe
      this.setState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
      });
    } catch (error) {
      console.error('Error creating shoe:', error);
    }
  }

  render() {
    const { manufacturer, model_name, color, picture_url, bin, bins } = this.state;

    return (
      <div>
        <h2>Add a New Shoe</h2>
        <form onSubmit={this.handleSubmit}>
          <label>
            Manufacturer:
            <input type="text" name="manufacturer" value={manufacturer} onChange={this.handleInputChange} />
          </label>
          <label>
            Model Name:
            <input type="text" name="model_name" value={model_name} onChange={this.handleInputChange} />
          </label>
          <label>
            Color:
            <input type="text" name="color" value={color} onChange={this.handleInputChange} />
          </label>
          <label>
            Picture URL:
            <input type="text" name="picture_url" value={picture_url} onChange={this.handleInputChange} />
          </label>
          <label>
            What closet would you like to store this entry in?:
            <select name="bin" value={bin} onChange={this.handleInputChange}>
              <option value="">select</option>
              {bins.map((bin) => (
                <option key={bin.id} value={bin.id}>
                 {bin.closet_name}
                </option>
              ))}
            </select>
            </label>
          <label>
            What bin would you like to store this entry in?:
            <select name="bin" value={bin} onChange={this.handleInputChange}>
              <option value="">select</option>
              {bins.map((bin) => (
                <option key={bin.id} value={bin.id}>
                 {bin.bin_number}
                </option>
              ))}
            </select>
          </label>
          <button type="submit">Add a New Shoe</button>
        </form>
      </div>
    );
  }
}

export default ShoeForm;
