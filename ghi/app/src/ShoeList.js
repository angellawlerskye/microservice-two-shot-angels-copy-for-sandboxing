import React, { Component } from 'react';

class ShoeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shoes: [],
      loading: true,
      deleteConfirmation: null,
    };
  }

  async componentDidMount() {
    try {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      this.setState({
        shoes: data.shoes,
        loading: false,
      });
    } catch (error) {
      console.error('Error fetching shoes:', error);
      this.setState({ loading: false });
    }
  }

  handleDelete = async (id) => {
    // Show the confirmation dialog
    this.setState({ deleteConfirmation: id });
  };

  confirmDelete = async () => {
    const { deleteConfirmation } = this.state;
    if (deleteConfirmation) {
      try {
        // Send a DELETE request to API endpoint to delete the shoe
        const response = await fetch(`http://localhost:8080/api/shoes/${deleteConfirmation}`, {
          method: 'DELETE',
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        // Remove the deleted shoe from the state
        this.setState((prevState) => ({
          shoes: prevState.shoes.filter((shoe) => shoe.id !== deleteConfirmation),
          deleteConfirmation: null, // Clear the confirmation dialog
        }));
      } catch (error) {
        console.error('Error deleting shoe:', error);
      }
    }
  };

  cancelDelete = () => {
    // Cancel the delete operation and hide the confirmation dialog
    this.setState({ deleteConfirmation: null });
  };

  render() {
    const { shoes, loading, deleteConfirmation } = this.state;

    return (
      <div>
        <h1>List of Shoes</h1>
        {loading ? (
          <p>Loading...</p>
        ) : (
          <ul>
            {shoes.map((shoe) => (
              <li key={shoe.id}>
                <h2>{shoe.model_name}</h2>
                <img src={shoe.picture_url} alt="Shoe.img" />

                {/* Dropdown to show all shoe details */}
                <details>
                <summary>Show Details</summary>
                <p>Manufacturer: {shoe.manufacturer}</p>
                <p>Color: {shoe.color}</p>
                <p>Current Location: {shoe.bin.closet_name}, Bin: {shoe.bin.bin_number}</p>
                </details>

                {/* Button to delete the shoe */}
                <button onClick={() => this.handleDelete(shoe.id)}>Delete</button>

                {deleteConfirmation === shoe.id && (
                  <div>
                    <p>Are you sure you want to delete this shoe?</p>
                    <button onClick={this.confirmDelete}>Yes</button>
                    <button onClick={this.cancelDelete}>No</button>
                  </div>
                )}
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

export default ShoeList;