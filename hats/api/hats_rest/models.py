from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.URLField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.CharField(max_length=10)
    shelf_number = models.CharField(max_length=10)


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField()

    location = models.ForeignKey(
        LocationVO,
        on_delete=models.PROTECT,
    )
