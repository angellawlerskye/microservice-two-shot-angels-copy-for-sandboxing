from django.urls import path
from .views import api_list_shoes, api_list_BinVOs, api_shoe_detail


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoe_detail, name="api_shoe_detail"),
    path("binvos/", api_list_BinVOs, name="api_list_BinVOs"),
]
