import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from .encoders import ShoeListEncoder, ShoeDetailEncoder, BinVOEncoder
# Create your views here.



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        # Get the Shoe object and put it in the content dict
        # try:
        id = content["bin"]
        bin = BinVO.objects.get(id=id)
        content["bin"] = bin
        # except BinVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid BinVO id"},
        #         status=400,
        #     )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, pk):
    shoe = Shoe.objects.filter(id=pk)
    if request.method == "GET":
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
        )
    else:
        count, _ = shoe.delete()
        return JsonResponse({"Deleted": count > 0})







@require_http_methods(["GET"])
def api_list_BinVOs(request):
    BinVOs = BinVO.objects.all()
    return JsonResponse(
        {"BinVOs": BinVOs},
        encoder=BinVOEncoder,
    )
